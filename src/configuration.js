import { configuration as config, configurationLayout as configLayout, configurationPack as configPack } from "@lgv/visualization-chart";
import packagejson from "../package.json";

const configuration = {
    branding: config.branding,
    name: packagejson.name.replace("/", "-").slice(1)
};

const configurationData = {
    delimiter: process.env.LGV_PARSE_DELIMITER || "."
}

const configurationLayout = {
    height: process.env.LGV_HEIGHT || configLayout.height,
    width: process.env.LGV_WIDTH || configLayout.width
};

const configurationPack = {
    padding: process.env.LGV_PACK_PADDING || configPack.padding,
};

export { configuration, configurationData, configurationLayout, configurationPack };
export default configuration;

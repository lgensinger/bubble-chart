import { BubbleLayout as BL, ChartLabel, LinearGrid } from "@lgv/visualization-chart";
import { select } from "d3-selection";
import { transition } from "d3-transition";

import { configuration, configurationData, configurationLayout } from "../configuration.js";

/**
 * BubbleChart is a area value visualization.
 * @param {class} BubbleLayout - lgv data class
 * @param {array} data - objects where each represents a path in the hierarchy
 * @param {integer} height - artboard height
 * @param {integer} width - artboard width
 */
class BubbleChart extends LinearGrid {
    constructor(data, width=configurationLayout.width, height=configurationLayout.height, padding=configurationLayout.padding, BubbleLayout=null, name=configuration.name, label=configuration.branding) {

        // initialize inheritance
        super(data, width, height, BubbleLayout ? BubbleLayout : new BL(data, { delimiter: configurationData.delimiter, height: height, padding: padding, width: width }), label, name);

        // update self
        this.bubble = null;
        this.classBubble = `${label}-bubble`;
        this.classLabel = `${label}-label`;
        this.classLabelPartial = `${label}-label-partial`;
        this.classNode = `${label}-node`;
        this.label = null;
        this.labelPartial = null;
        this.node = null;

    }

    /**
     * Position and minimally style bubble shapes in SVG dom element.
     */
    configureBubbles() {
        this.bubble
            .transition().duration(1000)
            .attr("class", this.classBubble)
            .attr("fill", "lightgrey")
            .attr("pointer-events", d => d.depth == 0 ? "none" : "all")
            .attr("r", d => this.Data.extractRadius(d));
    }

    /**
     * Position and minimally style labels in SVG dom element.
     */
    configureLabels() {
        this.label
            .attr("class", this.classLabel)
            .attr("data-label", d => d.data.label)
            .attr("text-anchor", "middle")
            .attr("pointer-events", "none")
            .attr("visibility", d => this.Label.determineHeight(d.data.label) * 2 > d.r * 2 ? "hidden" : "visible")
            .attr("x", 0)
            .attr("y", 0);
    }

    /**
     * Position and minimally style label partials in SVG dom element.
     */
    configureLabelPartials() {
        this.labelPartial
            .transition().duration(300)
            .attr("class", this.classLabelPartial)
            .attr("x", 0)
            .attr("dy", (d,i) => this.Label.calculateDy(i, d, d.r * 2))
            .textTween((d,i) => this.tweenText(d.data.label, d.data.value, d.r * 2, i));
    }

    /**
     * Position and minimally style bubbles in SVG dom element.
     */
    configureBubbleEvents() {
        this.bubble
            .on("click", (e,d) => this.configureEvent("bubble-click",d,e))
            .on("mouseover", (e,d) => {

                // update class
                select(e.target).attr("class", `${this.classBubble} active`);

                // send event to parent
                this.configureEvent("bubble-mouseover",d,e);

            })
            .on("mouseout", (e,d) => {

                // update class
                select(e.target).attr("class", this.classBubble);

                // send event to parent
                this.artboard.dispatch("bubble-mouseout", {
                    bubbles: true
                });

            });
    }

    /**
     * Position and minimally style nodes in SVG dom element.
     */
    configureNodes() {
        this.node
            .transition().duration(1000)
            .attr("class", this.classNode)
            .attr("data-id", d => d.data.id)
            .attr("data-label", d => d.data.label)
            .attr("transform", d => `translate(${d.x},${d.y})`);
    }

    /**
     * Generate bubble shapes in SVG element.
     * @param {node} domNode - d3.js SVG selection
     */
    generateBubbles(domNode) {
        return domNode
            .selectAll(`.${this.classBubble}`)
            .data(d => d)
            .join(
                enter => enter.append("circle"),
                update => update,
                exit => exit.remove().transition().duration(1000)
            );
    }

    /**
     * Generate bubble labels in SVG element.
     * @param {node} domNode - d3.js SVG selection
     */
    generateLabels(domNode) {
        return domNode
            .selectAll(`.${this.classLabel}`)
            .data(d => d)
            .join(
                enter => enter.append("text"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate bubble label partials in SVG element.
     * @param {node} domNode - d3.js SVG selection
     */
    generateLabelPartials(domNode) {
        return domNode
            .selectAll(`.${this.classLabelPartial}`)
            .data(d => [d, d])
            .join(
                enter => enter.append("tspan"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Construct bubble group in HTML DOM.
     * @param {selection} domNode - d3 selection
     * @returns A d3.js selection.
     */
    generateNodes(domNode) {
        return domNode
            .selectAll(`.${this.classNode}`)
            .data(this.data ? this.data.leaves() : [])
            .join(
                enter => enter.append("g"),
                update => update,
                exit => exit.transition()
                    .duration(1000)
                    .attr("transform", `translate(${this.width / 2},${this.height / 2}) scale(0)`)
                    .remove()
            );
    }

    /**
     * Generate visualization.
     */
    generateChart() {

        // initialize labeling
        this.Label = new ChartLabel(this.artboard, this.Data);

        // generate group for each bubble
        this.node = this.generateNodes(this.content);
        this.configureNodes();

        // generate bubbles
        this.bubble = this.generateBubbles(this.node);
        this.configureBubbles();
        this.configureBubbleEvents();

        // generate labels
        this.label = this.generateLabels(this.node);
        this.configureLabels();

        // generate label partials so they stack
        this.labelPartial = this.generateLabelPartials(this.label);
        this.configureLabelPartials();

    }

};

export { BubbleChart };
export default BubbleChart;

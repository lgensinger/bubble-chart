# Bubble Chart

ES6 d3.js bubble chart visualization.

## Install

```bash
# install package
npm install @lgv/bubble-chart
```

## Data Format

The following values are the expected input data structure; id can be omitted.

```json
[
    {
        "id": 1,
        "label": "xyz",
        "value": 1
    },
    {
        "id": 2,
        "label": "abc",
        "value": 3
    }
]
```

## Use Module

```bash
import { BubbleChart } from "@lgv/bubble-chart";

// have some data
let data = [
    {
        "id": 1,
        "label": "xyz",
        "value": 1
    },
    {
        "id": 2,
        "label": "abc",
        "value": 3
    }
];

// initialize
const bc = new BubbleChart(data);

// render visualization
bc.render(document.body);
```

## Environment Variables

The following values can be set via environment or passed into the class.

| Name | Type | Description |
| :-- | :-- | :-- |
| `LGV_HEIGHT` | integer | height of artboard |
| `LGV_WIDTH` | integer | width of artboard |

## Events

The following events are dispatched from the svg element. Hover events toggle a class named `active` on the element.

| Target | Name | Event |
| :-- | :-- | :-- |
| bubble circle | `bubble-click` | on click |
| bubble circle | `bubble-mouseover` | on hover |
| bubble circle | `bubble-mousemout` | on un-hover |

## Style

Style is expected to be addressed via css. Any style not met by the visualization module is expected to be added by the importing component.

| Class | Element |
| :-- | :-- |
| `lgv-bubble` | circle element |
| `lgv-bubble-chart` | top-level svg element |
| `lgv-bubble-label` | circle text label element |
| `lgv-bubble-label-partial` | individual stacked lines of circle text label element |
| `lgv-node` | circle/text group element |

## Actively Develop

```bash
# clone repository
git clone <repo_url>

# update directory context
cd bubble-chart

# run docker container
docker run \
  --rm \
  -it  \
  -v $(pwd):/project \
  -w /project \
  -p 8080:8080 \
  node \
  bash

# FROM INSIDE RUNNING CONTAINER

# install module
npm install .

# run development server
npm run startdocker

# edit src/index.js
# add const sbc = new BubbleChart(data);
# add sbc.render(document.body);
# replace `data` with whatever data you want to develop with

# view visualization in browser at http://localhost:8080
```

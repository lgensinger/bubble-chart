import path from "path";
import { dirname } from "path";
import { fileURLToPath } from "url";

import webpack from "webpack";

const __dirname = dirname(fileURLToPath(import.meta.url));

const commonConfig = {

    entry: {
        index: "./src/index.js"
    },

    plugins: [
        new webpack.DefinePlugin({
            "process.env": {
                "LGV_HEIGHT": JSON.stringify(process.env.LGV_HEIGHT),
                "LGV_PADDING": JSON.stringify(process.env.LGV_PADDING),
                "LGV_PACK_PADDING": JSON.stringify(process.env.LGV_PACK_PADDING),
                "LGV_PARSE_DELIMITER": JSON.stringify(process.env.LGV_PARSE_DELIMITER),
                "LGV_WIDTH": JSON.stringify(process.env.LGV_WIDTH)
            }
        })
    ],

    output: {
        filename: "bubble-chart.bundle.js",
        path: path.resolve(__dirname, "dist"),
        clean: true
    }

 };

 export { commonConfig };
 export default commonConfig;
